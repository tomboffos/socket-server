function socketServer(){
    io.on("connection", (socket) => {
        console.log("User connected " + socket.id)

        socket.on('message',(data)=>{
            socket.broadcast.emit('message',data)
        })
    })
}