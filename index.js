const express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io')(server, {
        cors: {
            origin: "*"
        }
    });
app.use(express.json())
app.set('io',io)
app.set('view engine', 'ejs')
app.get('/', (req, res) => {
    res.render('index',{})
})
app.post('/',(req,res)=>{
    io.emit(req.body.event,req.body.data)
    console.log('data is ',req.body);

    res.json({
        "success" : 200
    })
})

server.listen(4000)
